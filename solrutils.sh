#!/bin/sh
#
# author: rayh
# 
# Reload the core
# Gets status of core
# Performs data import
# Moves config from wotkit project ($WOTKIT_DIR) to solr cores ($SOLR_CORES)

usage() {
  echo `basename $0`: ERROR: $* 1>&2
  echo usage: `basename $0` 'reload|status|di-s|di-f|di-d|mv-conf
      [core ...]' 1>&2
  exit 1
}

case "$1" in
  # TODO make this log to a file instead of barfing at the main screen
  start) 
    cd "$SOLR_DIR/example"
    java -Duser.timezone=UTC -jar start.jar & ;;
  stop)  ps aux | grep start.jar | head -1 | awk '{print $2}' | xargs kill ;;
  reload) curl -s "http://localhost:8983/solr/admin/cores?action=RELOAD&core=$2&wt=json" | python -mjson.tool ;;
  status) curl -s "http://localhost:8983/solr/admin/cores?action=STATUS&core=$2&wt=json" | python -mjson.tool ;;
  di-s)  curl -s "http://localhost:8983/solr/$2/dataimport&wt=json" | python -mjson.tool ;;
  di-f) curl -s "http://localhost:8983/solr/$2/dataimport?command=full-import&clean=true&commit=true&wt=json" | python -mjson.tool ;;
  di-d) curl -s "http://localhost:8983/solr/$2/dataimport?command=delta-import&commit=true&wt=json" | python -mjson.tool ;;
  mv-conf) 
    CONFIGS=($WOTKIT_DIR/wotkit-solr/wotkit/conf/{data-config.xml,schema.xml})
    cp "${CONFIGS[@]}" "$SOLR_DIR/example/solr/$2/conf"
    ;;
  *) usage "bad argument $1";;
esac

