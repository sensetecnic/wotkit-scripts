# Copyright (c) 2010 Sense Tecnic Systems Inc.
#
# Shell script to stop any running Python and Java sensors scripts started with start-sts-sensors.sh
#
# Usage: ./stop-sts-sensors.sh
# Author: Vincent Tsao

SENSOR_SCRIPT_HOME=/home/vtsao/StsSensors

echo Stopping any running sensors...

awk 'NR > 3 {system("kill " $0) system("echo killing sensor with pid " $0)}' $SENSOR_SCRIPT_HOME/running.txt
rm $SENSOR_SCRIPT_HOME/running.txt

echo Done.

exit 0

