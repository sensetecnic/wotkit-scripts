# Copyright (c) 2010 Sense Tecnic Systems Inc.
#
# Shell script to start all the default Python and Java sensor scripts for Sense Tecnic.
# This script starts the following:
#    - fleet tracker
#    - Mike's test sensor
#    - Rodger's sensors
#    - phidget sensors
#    - Ivy's sensors
#    - servo actuator
#
# ***Ensure the OSGiBroker is started before executing this script!***
#
# Usage: ./start-sts-sensors.sh
# Author: Vincent Tsao

SENSOR_SCRIPT_HOME=/home/vtsao/StsSensors

# stop any currently running sensor scripts
$SENSOR_SCRIPT_HOME/stop-sts-sensors.sh
echo This is an automatically generated file, do not create, edit, or remove this file manually. >> running.txt
echo This file contains the pid\'s of all currently running Sense Tecnic sensor scripts. >> running.txt
echo This file\'s life and death are managed by start-sts-sensors.sh and stop-sts-sensors.sh >> running.txt

# start the fleet tracker
cd $SENSOR_SCRIPT_HOME/FleetTracker
echo "Starting the fleet tracker..."
java -jar ./FleetTracker.jar > /dev/null&
fleetPid="${!}"
echo $fleetPid >> $SENSOR_SCRIPT_HOME/running.txt
echo "Fleet tracker started, pid is $fleetPid"

# start Mike's test sensor
cd $SENSOR_SCRIPT_HOME/TestSensor
echo "Starting Mike's test sensor..."
python ./TestSensor.py > /dev/null&
testPid="${!}"
echo $testPid >> $SENSOR_SCRIPT_HOME/running.txt
echo "Test sensor started, pid is $testPid"

# start Rodger's sensors
cd $SENSOR_SCRIPT_HOME/RodgersSensors
echo "Starting Rodger's sensors..."
python ./cpu.py > /dev/null&
cpuPid="${!}"
echo $cpuPid >> $SENSOR_SCRIPT_HOME/running.txt
echo "CPU sensor started, pid is $cpuPid"

python ./eq.py > /dev/null&
eqPid="${!}"
echo $eqPid >> $SENSOR_SCRIPT_HOME/running.txt
echo "Earthquake sensor started, pid is $eqPid"

python ./ferry.py > /dev/null&
ferryPid="${!}"
echo $ferryPid >> $SENSOR_SCRIPT_HOME/running.txt
echo "Ferry sensor started, pid is $ferryPid"

python ./UBC-elec.py > /dev/null&
ubcelecPid="${!}"
echo $ubcelecPid >> $SENSOR_SCRIPT_HOME/running.txt
echo "UBC electricity sensor started, pid is $ubcelecPid"

python ./uk-elec.py > /dev/null&
ukelecPid="${!}"
echo $ukelecPid >> $SENSOR_SCRIPT_HOME/running.txt
echo "UK electricity sensor started, pid is $ukelecPid"

python ./yvr.py > /dev/null&
yvrPid="${!}"
echo $yvrPid >> $SENSOR_SCRIPT_HOME/running.txt
echo "YVR sensor started, pid is $yvrPid"

# start the phidgets
cd $SENSOR_SCRIPT_HOME/PhidgetSensors
echo "Starting the phidget sensors..."
python ./PhidgetSensorDriver.py > /dev/null&
phidgetPid="${!}"
echo $phidgetPid >> $SENSOR_SCRIPT_HOME/running.txt
echo "Phidget sensors started, pid is $phidgetPid"

# start Ivy's sensors
cd $SENSOR_SCRIPT_HOME/SensorWebScrapes
echo "Starting Ivy's sensors..."
python ./AirCareWaitTimes.py > /dev/null&
aircareWaitPid="${!}"
echo $aircareWaitPid >> $SENSOR_SCRIPT_HOME/running.txt
echo "Aircare wait times sensor started, pid is $aircareWaitPid"

python ./AirQualityGibraltar.py > /dev/null&
airQualityGibPid="${!}"
echo $airQualityGibPid >> $SENSOR_SCRIPT_HOME/running.txt
echo "Air quality gibraltar sensor started, pid is $airQualityGibPid"

python ./AirQualityUS.py > /dev/null&
airQualityUSPid="${!}"
echo $airQualityUSPid >> $SENSOR_SCRIPT_HOME/running.txt
echo "Air quality US sensor started, pid is $airQualityUSPid"

python ./AverageGasPrices.py > /dev/null&
averageGasPid="${!}"
echo $averageGasPid >> $SENSOR_SCRIPT_HOME/running.txt
echo "Average gas prices sensor started, pid is $averageGasPid"

# start the servero actuator
cd $SENSOR_SCRIPT_HOME/ServoActuator/src
echo "Starting the servo actuator..."
python ./servo-actuator.py > /dev/null&
servoActuatorPid="${!}"
echo $servoActuatorPid >> $SENSOR_SCRIPT_HOME/running.txt
echo "Servo actuator started, pid is $servoActuatorPid"

# helper function to kill all processes created by this script
cleanUp() {
  echo "Script exiting, stopping all processes..."

  kill $fleetPid
  kill $testPid
  kill $cpuPid
  kill $eqPid
  kill $ferryPid
  kill $ubcelecPid
  kill $ukelecPid
  kill $yvrPid
  kill $phidgetPid
  kill $aircareWaitPid
  kill $airQualityGibPid
  kill $airQualityUSPid
  kill $averageGasPid
  kill $servoActuatorPid

  rm $SENSOR_SCRIPT_HOME/running.txt

  echo "Done."
}

trap 'cleanUp;' INT
wait $fleetPid
wait $testPid
wait $cpuPid
wait $eqPid
wait $ferryPid
wait $ubcelecPid
wait $ukelecPid
wait $yvrPid
wait $phidgetPid
wait $aircareWaitPid
wait $airQualityGibPid
wait $airQualityUSPid
wait $averageGasPid
wait $servoActuatorPid

exit 0

