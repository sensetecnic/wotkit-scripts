#!/bin/bash
clear
echo "WotKit Build Start"
cd ../wotkit-parent

#mvn clean install

mkdir ~/wot-kit
echo "copying created files for deployment"
mkdir ~/wot-kit/web
cp ../wotkit/target/wotkit-*.war ~/wot-kit/web
mkdir ~/wot-kit/web/db
cp ../wotkit/db/sql/*.sql ~/wot-kit/web/db

mkdir ~/wot-kit/processor
cp ../processor/target/processor-*.war ~/wot-kit/processor
mkdir ~/wot-kit/processor/db
cp ../processor/db/mysql/*.sql ~/wot-kit/processor/db

tar cvzf ~/wot-kit.tgz ~/wot-kit 
