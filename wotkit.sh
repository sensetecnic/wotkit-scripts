#!/bin/sh
#
# author: rayh
# 
# database dumps for all!

usage() {
  echo `basename $0`: ERROR: $* 1>&2
  echo usage: `basename $0` 'start|dump|cp-jsp
    dump [all|tables|data]
    build
    load' 1>&2
  exit 1
}

case "$1" in
  start)
    activemq start
    sudo mysqld_safe &
    catalina.sh jpda start
    cd "$SOLR_DIR/example"
    java -Duser.timezone=UTC -jar start.jar &
    ;;
  build)
    cd "$WOTKIT_DIR/wotkit-parent"
    mvn -o -pl :wotkit-core,:api install tomcat6:redeploy -Dmaven.test.skip=true -T 2
    ;;
  dump)
    case "$2" in
      all) mysqldump -u root -p $3 > dump.sql ;;
      tables) mysqldump --no-data -u root -p $3 > tables.sql;;
      data)   mysqldump --skip-extended-insert --no-create-info -u root -p $3 > data.sql;;
      *)  usage "bad argument $1 $2 $3";;
    esac
    ;;
  load)
     # load database ??

    ;;
  cp-jsp) usage "not yet defined $1";;
  *) usage "bad argument $1";;
esac

